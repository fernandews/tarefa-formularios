const deleteBook = document.querySelector('#table-books');

deleteBook.addEventListener('click', (e) => {
    let id = e.target.parentElement.firstChild.innerText
    if(e.target.className == 'delete'){
        fetch(`${url}/${id}`, {
            method: 'DELETE'
        }).then( () => {
        trDelete = e.target.parentElement.parentElement
        trDelete.removeChild(e.target.parentElement)}
        ).catch((err) => {
            alert(err);
        })
    }
});

const url = 'https://treinamento-api.herokuapp.com/books'
function deleteBookById (id) {
    fetch(`${url}/${id}`, {
        method: 'DELETE'
    }).then((response) => {
        console.log(response);
    }).catch((err) => {
        alert(err);
    })
}
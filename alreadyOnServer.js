// AJEITA A TABELA
const parseRequestToJSON = (requestResult) => {
    return requestResult.json();
}

const addToUL = (responseAsJSON) => {
    for (i = 0; i < responseAsJSON.length; i += 1) {
        const title = document.createElement('td');
        title.innerHTML = `${responseAsJSON[i].name}`;
        const author = document.createElement('td');
        author.innerHTML = `${responseAsJSON[i].author}`;
        const created = document.createElement('td');
        created.innerHTML = `${responseAsJSON[i].created_at}`;
        const updated = document.createElement('td');
        updated.innerHTML = `${responseAsJSON[i].updated_at}`;

        const deleteBtn = document.createElement('td');
        deleteBtn.className = 'delete';
        deleteBtn.innerText = 'excluir';

        const id = document.createElement('td');
        id.className = 'id'
        id.innerHTML = `${responseAsJSON[i].id}`;

        const tableRow = document.createElement('tr');
        tableRow.appendChild(id);
        tableRow.appendChild(title);
        tableRow.appendChild(author);
        tableRow.appendChild(created);
        tableRow.appendChild(updated);
        tableRow.appendChild(deleteBtn);

        const bookList = document.querySelector('#table-books');
        bookList.appendChild(tableRow);
    };
};

fetch('https://treinamento-api.herokuapp.com/books/')
.then (parseRequestToJSON)
.then(addToUL);
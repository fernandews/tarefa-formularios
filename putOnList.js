// Adicionado pelo form
const add = document.querySelector('#add-btn');
const newBookName = document.querySelector('#book-name');
const newBookAuthor = document.querySelector('#book-author');

add.addEventListener('click', (e) => {
    e.preventDefault();
});


function postForm(book) {
    fetch('https://treinamento-api.herokuapp.com/books', {
        method: "post",
        body: JSON.stringify(book),
        headers: {
            "Content-Type": "application/json"
        }
    }).then(parseRequestToJSON).then( (responseAsJSON) => {
    const title = document.createElement('td');
    title.innerHTML = `${responseAsJSON.name}`;
    const author = document.createElement('td');
    author.innerHTML = `${responseAsJSON.author}`;
    const created = document.createElement('td');
    created.innerHTML = `${responseAsJSON.created_at}`;
    const updated = document.createElement('td');
    updated.innerHTML = `${responseAsJSON.updated_at}`;
    
    const deleteBtn = document.createElement('td');
    deleteBtn.className = 'delete';
    deleteBtn.innerText = 'excluir';
    
    const id = document.createElement('td');
    id.className = 'id'
    id.innerHTML = `${responseAsJSON.id}`;
    
    const tableRow = document.createElement('tr');
    tableRow.appendChild(id);
    tableRow.appendChild(title);
    tableRow.appendChild(author);
    tableRow.appendChild(created);
    tableRow.appendChild(updated);
    tableRow.appendChild(deleteBtn);
    
    const bookList = document.querySelector('#table-books');
    bookList.appendChild(tableRow);
    })
};

add.addEventListener('click', (e) => {
    const bookName = newBookName.value
    const bookAuthor = newBookAuthor.value
    const book = {
        "name": `${bookName}`,
        "author": `${bookAuthor}`
    }
    postForm(book)
})
